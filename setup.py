
from setuptools import setup, find_packages
from liftoff.core.version import get_version

VERSION = get_version()

f = open('README.md', 'r')
LONG_DESCRIPTION = f.read()
f.close()

setup(
    name='liftoff',
    version=VERSION,
    description='Command line tools for creating local development projects and resources',
    long_description=LONG_DESCRIPTION,
    long_description_content_type='text/markdown',
    author='Helium, LLC',
    author_email='developers@heliumservices.com',
    url='https://bitbucket.org/teamhelium/com.helium.liftoff.cli/',
    license='MIT',
    packages=find_packages(exclude=['ez_setup', 'tests*']),
    package_data={'liftoff': ['templates/*']},
    include_package_data=True,
    entry_points="""
        [console_scripts]
        liftoff = liftoff.main:main
    """,
)
